function multiplier(factor) {
	console.log(`Passing factor ${factor} to multiplier`)
	return function (base){
		console.log(`called with base number ${base}, received factor ${factor} from enclosing function`);
		return base * factor;
	}
}
let double = multiplier(2);
let triple = multiplier(3)
console.log(double(5));
console.log(triple(4))