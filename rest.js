function printall (array){
	for (let arrayElement of array) {
		console.log(arrayElement)
	}
}

function printrest (first,...array){
	for (let arrayElement of array) {
		console.log(arrayElement)
	}
}
printall(["red","green","blue"])
console.log("==============")
printrest("red","green","blue")

let colors = ["red","green","blue"];
let secondary=["violet","yellow","cyan"]
//colors.push(secondary)
for (let string of secondary) {
	colors.push(string)
}

console.log(colors)
colors.push(...secondary)


let today = {
	day: 19,
	month: "april",
	year:2021
}
let day1 = {
	squirrel: false,
	events: ["work", "touched tree", "pizza", "running"],
	date:today
};
console.log(day1)

 day1 = {
	squirrel: false,
	events: ["work", "touched tree", "pizza", "running"],
	...today
};

console.log(day1)

