const day1 = {
	squirrel: false,
	events: ["work", "touched tree", "pizza", "running"]
};

console.log(day1.squirrel)
console.log(day1.events)

day1.weather="sunny"
console.log(day1)
day1.date = {
	day: 19,
	month: "april",
	year:2021
}
console.log(day1)
day1.date.weekday="monday"
console.log(day1)
console.log(day1.headlines)
day1.headlines = ["Fire in Anderlecht","Anderlecht joins eurocompetition"]
console.log(day1.headlines)
console.log(day1["headlines"])
day1["2"]="two"
console.log(day1)
console.log(day1[2])
for(let key of Object.keys(day1)){
	console.log(key)
}

