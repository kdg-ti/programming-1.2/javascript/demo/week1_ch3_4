const sequence = [1, 2, 3];
sequence.push(5)
console.log(sequence);
sequence.pop()
console.log(sequence);
sequence.unshift(6)
console.log(sequence);
sequence.shift()
console.log(sequence);

for(let i = 0;i<sequence.length;i++){
  console.log(sequence[i]);
}

for (let number of sequence) {
  console.log(number)
}
console.log(thing)
newSequence=sequence.concat([5,6,7]);
console.log(newSequence);
console.log(newSequence.slice(2));

let variable = sequence
variable.push(1000)
console.log(variable)
variable = [100,101,102]
console.log(variable)

const constant = sequence
constant.push(1000)
constant[2]="jan"
console.log(constant)
// reassiging to a const is not allowed!!
constant = [100,101,102]
console.log(constant)
